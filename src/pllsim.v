`timescale 1ps/1ps

module PLL
(
    input  wire clkin,   // 25 MHz, 0 deg
    output wire pll50,   // 50 MHz, 0 deg
    output wire pll100,  // 100 MHz, 0 deg
    output wire locked
);

    reg  clk1 = 0, clk2 = 0;
    always #10000 clk1 = !clk1;
    always #2500  clk2 = !clk2;

    assign pll50  = clk1;
    assign pll100 = clk2;
    assign locked = 1;

endmodule
