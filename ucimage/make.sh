#!/bin/sh

ia entry.s
ic main.c
ic spi.c
ic simrv.c
il -H1 -l -t -T0x10000000 -R8 -c entry.i main.i spi.i simrv.i
il -H1 -l -t -a -T0x10000000 -R8 -c entry.i main.i spi.i simrv.i >list.txt
hexdump -e '1/4 "%08x\n"' -n 8192 -v i.out >ucmem.hex

