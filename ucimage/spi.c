
#define FAST 0x00000002
#define SEL  0x00000001

int data;

int *spi_ctrl = (int*) 0x4000d000;
int *spi_data = (int*) 0x4000d004;

void spi_clr(int n)
{
	*spi_ctrl = 0;
	while(n--) {
		*spi_data = -1;
		data = *spi_data;
	}
}
  
void spi(int val)
{
	*spi_ctrl = SEL;
	*spi_data = val;
	data = *spi_data;
}
  
void spi_cmd(int cmd, int arg)
{
	int i, crc;

	do { spi_clr(1); } while (data!=255);
	do { spi(1);     } while (data!=255);
	switch(cmd) {
		case 0:  crc = 149; break;
		case 8:  crc = 135; break;
		default: crc = 255;
	}
	spi((cmd & 0x3f) | 0x40);
	spi((arg>>24) & 0xff);
	spi((arg>>16) & 0xff);
	spi((arg>> 8) & 0xff);
	spi( arg      & 0xff);
	spi(crc);
	for(i=0; i<32; i++) {
		spi(255);
		if(data < 0x80) break;
	}
}
  
void spi_init(void)
{
	int rc, data;
	spi_clr(9);        // idle for 80+ clocks
	spi_cmd(0, 0);     // set MMC spi mode
	spi_cmd(8, 0x1aa); // CMD8
	spi(-1); spi(-1); spi(-1);
	do {
		// REPEAT ACMD41, start init, until okay
		spi_cmd(55, 0); spi_cmd(41, 1<<30);
		rc = *spi_data;
		spi(-1); spi(-1); spi(-1);
		spi_clr(10000);
	} while (rc != 0);
	spi_cmd(16, 512);
	spi_clr(1);
}

int is_hc(void)
{
	int i, data;
	
	spi_cmd(58, 0); // CM58 = get capacity
	data = *spi_data;
	spi(-1);
	i = (data!=0 || (*spi_data & 0x20)) ? 1 : 0;
	spi(-1); spi(-1); spi_clr(1);
	return i;
}

void read_sd(int sec, int *buf)
{	
	int i;

	if(!is_hc()) sec <<= 9;
	spi_cmd(17, sec); // CMD17 = read sector
	do { spi(-1); } while (data!=254);
	*spi_ctrl = FAST|SEL;
	for(i=0; i<512; i+=4) {
		*spi_data = -1;
		*buf++ = *spi_data;
	}
	spi(255); spi(255); spi_clr(1);
}

void write_sd(int sec, int *buf)
{
	int i;
	
	if(!is_hc()) sec <<= 9;
	spi_cmd(24, sec); // CMD24 = write sector
	spi(254);
	*spi_ctrl = FAST|SEL;
	for(i=0; i<512; i+=4) {
		*spi_data = *buf++;
	}
	spi(255); spi(255);
	for(i=0; i<10000; i++) {
		spi(-1);
		if((data & 0x1f) == 5) break;
	}
	spi_clr(1);
}
