/******************************************************************************************/
/**** SimCore/RISC-V since 2018-07-05                             ArchLab. TokyoTech   ****/
/******************************************************************************************/
//#include <cstdint.h>

#include <stdint.h>
#include "simrv.h"

//volatile uint8_t *HOGE = (uint8_t *)0x0;
#define DISK_DEBUG 1
//#include "define.h"
/**** VirtIO 0x40000000 ~                                                              ****/
/******************************************************************************************/
#define VIRTIO_BASE_ADDR (0x40000000) // NotChange
#define VIRTIO_SIZE      (0x08000000) //
#define VRING_DESC_F_NEXT     (1)
#define VRING_DESC_F_WRITE    (2)
#define VRING_DESC_F_INDIRECT (4)

/* console */
#define CONSOLE_MAX_QUEUE_NUM (2)
#define VIRTIO_CONSOLE_IRQ (1)

/* block device (disk) */
#define SECTOR_SIZE         (512)
#define DISK_BUF_SIZE       (512 * 512)
#define DISK_SIZE           (64 * 1024 * 1024)
#define DISK_MAX_QUEUE_NUM  (4)
#define VIRTIO_DISK_IRQ     (2)
#define VIRTIO_BLK_T_IN     (0)
#define VIRTIO_BLK_T_OUT    (1)
#define VIRTIO_BLK_S_OK     (0)
#define VIRTIO_BLK_S_IOERR  (1)
#define VIRTIO_BLK_S_UNSUPP (2)

/**** PLIC (Platform-Level Interrupt Contoroller) 0x50000000 ~                         ****/
/******************************************************************************************/
#define PLIC_BASE_ADDR   (0x50000000) // NotChange:
#define PLIC_SIZE        (0x00400000)
#define PLIC_HART_BASE   (0x200000)
#define PLIC_HART_SIZE   (0x1000)

/**** CLINT (Core Local Interruputer) 0x60000000 ~                                     ****/
/******************************************************************************************/
#define CLINT_BASE_ADDR  (0x60000000) // NotChange:
#define CLINT_SIZE       (0x000c0000) //

/**** DRAM (Main Memory) 0x80000000 ~                                                  ****/
/******************************************************************************************/
#define DRAM_BASE_ADDR (0x80000000)
#define DRAM_SIZE      (64 * 1024 * 1024)
#define DRAM_MASK      (0x3fffffff)
#define D_PAGE_SHIFT   (12)          // page shift for page size of 4KB
#define D_PAGE_MASK    (0x00000fff)  // page mask  for page size of 4KB
#define TLB_SIZE       (4)


/******************************************************************************************/
#define DISK_MAGIC_VALUE       0x74726976
#define DISK_VERSION           2
#define DISK_DEVIDE_ID         2
#define DISK_VENDOR_ID         0xffff
#define DISK_DEVICE_FEATURES   1
#define DISK_CONFIG_GENERATION 0
#define DISK_QUEUE_NUM_MAX     4

#pragma pack on
typedef struct QueueState {
    uint32_t Ready;
    uint32_t Notify;
    uint32_t DescLow;
    uint32_t DescHigh;
    uint32_t AvailLow;
    uint32_t AvailHigh;
    uint32_t UsedLow;
    uint32_t UsedHigh;
    uint32_t last_avail_idx;
}QueueState;
#pragma pack 0

typedef struct BlockRequestHeader {
    uint32_t type;
    uint32_t ioprio;
    uint32_t sector_num;
    uint32_t sector_numh;
}BlockRequestHeader;

typedef struct Descriptor {
    uint32_t adr;
    uint32_t adrh;
    uint32_t len;
    uint16_t flags;
    uint16_t next;
}Descriptor;

int disk_debug_num;

/******************************************************************************************/
uint32_t ram_ld(uint32_t addr, int n, uint8_t *ram){
    if(n!=1 && n!=2 && n!=4){
        //printf("__ Error: ram_r() not supported n=%d\n", n);
        simrv_puts("__ Error: ram_r() not supported n=");
        simrv_puth(n);
        simrv_puts("\n");
        simrv_exit(0);
    }
    uint32_t data = 0;
    for (int i=0; i<n; i++) {
        //data |= ((uint32_t)ram[(addr + i) & DRAM_MASK]) << (8*i);     // NOTE!
        data |= ((uint32_t)ram[(addr + i) /*& DRAM_MASK*/]) << (8*i);
    }

    return data;
}
/******************************************************************************************/
void ram_st(uint32_t addr, uint32_t data, int n, uint8_t *ram){
    if(n!=1 && n!=2 && n!=4){
        //printf("__ Error: dsk_w() not supported n=%d\n", n);
        simrv_puts("__ Error: dsk_w() not supported n=");
        simrv_puth(n);
        simrv_puts("\n");
        simrv_exit(0);
    }
    if(n==1){
        ram[addr /*& DRAM_MASK*/] = data & 0xff;
    }
    else if (n==2){
        ram[ addr    /*& DRAM_MASK*/] =  data       & 0xff;
        ram[(addr+1) /*& DRAM_MASK*/] = (data >> 8) & 0xff;
    }
    else if (n==4){
        ram[ addr    /*& DRAM_MASK*/] =  data       & 0xff;
        ram[(addr+1) /*& DRAM_MASK*/] = (data >> 8) & 0xff;
        ram[(addr+2) /*& DRAM_MASK*/] = (data >>16) & 0xff;
        ram[(addr+3) /*& DRAM_MASK*/] = (data >>24) & 0xff;
    }
}


/*** update the used ring                                                              ****/
/******************************************************************************************/
void update_descriptor(uint32_t desc_idx, uint32_t desc_len, int q_num, 
                       QueueState *qs, uint8_t *mmem){
    uint32_t addr_used_idx = qs->UsedLow + 2;
    uint32_t index = (uint16_t)ram_ld(addr_used_idx, 2, mmem);

    ram_st(addr_used_idx, index+1, 2, mmem);

    uint32_t addr_used_entry = qs->UsedLow + 4 + (index & (q_num - 1)) * 8;
    ram_st(addr_used_entry,   desc_idx, 4, mmem);
    ram_st(addr_used_entry+4, desc_len, 4, mmem);
}

/******************************************************************************************/

#define DESC_SIZE 16 /* descriptor size 16 byte */

/******************************************************************************************/
/*** console_request for display output                                                ****/
/******************************************************************************************/
void cons_request(uint8_t *mmem, uint32_t q_num, QueueState *qs){
    Descriptor desc;
    uint8_t *p;
    disk_debug_num++; /* just for debug */

    uint16_t avail_idx = (uint16_t)ram_ld(qs->AvailLow+2, 2, mmem);
    while (qs->last_avail_idx != avail_idx) { ///// Note!!

        uint32_t adr = qs->AvailLow + 4 + (qs->last_avail_idx & (q_num - 1)) * 2;
        uint16_t desc_idx_header = ram_ld(adr, 2, mmem);
        uint32_t desc_adr_header = desc_idx_header * DESC_SIZE + qs->DescLow;

        p = (uint8_t*)&desc;
        for(int i=0; i<DESC_SIZE; i++){ *p = ram_ld(desc_adr_header+i, 1, mmem); p++; }

        for(int i=0; i<(int)desc.len; i++){ /***** write to stdout *****/
            uint8_t d = ram_ld(desc.adr+i, 1, mmem);
            simrv_putc(d);
        }

        update_descriptor(desc_idx_header, 0, q_num, qs, mmem);
        qs->last_avail_idx++;
    }
}

/******************************************************************************************/
/*** disc sector read & write                                                          ****/
/******************************************************************************************/
read_sd(int sec, int buf);
write_sd(int sec, int buf);
void spi_init(void);

void disk_request(uint8_t *mmem, uint8_t *mdsk, int q_num, QueueState *qs){
    Descriptor desc;
    BlockRequestHeader header;
    uint8_t *p;
    int j;
    static int need_init = 1;

    disk_debug_num++; /* just for debug */
    if(need_init) {
	spi_init();
	need_init = 0;
    }

    uint16_t avail_idx = *(uint16_t*)(qs->AvailLow+2);
    while (qs->last_avail_idx != avail_idx) { /***** header -> sector -> footer *****/

        // (1) header
        uint32_t adr = qs->AvailLow + 4 + (qs->last_avail_idx & (q_num - 1)) * 2;
        uint16_t desc_idx_header = ram_ld(adr, 2, mmem);
        uint32_t desc_adr_header = desc_idx_header * DESC_SIZE + qs->DescLow;

        p = (uint8_t*)&desc;
        for(int i=0; i<DESC_SIZE; i++){ *p = ram_ld(desc_adr_header+i, 1, mmem); p++; }

        p = (uint8_t*)&header;
        for(int i=0; i<(int)desc.len; i++){ *p = ram_ld(desc.adr+i, 1, mmem); p++; }
        if (desc.len!=16) { simrv_puts("__ ERROR: disk_request() desc.len!=16\n"); simrv_exit(0); }

        // (2) sector
        uint16_t desc_idx_sector = desc.next;
        uint32_t desc_adr_sector = desc_idx_sector * DESC_SIZE + qs->DescLow;
        p = (uint8_t*)&desc;
        for(int i=0; i<DESC_SIZE; i++){ *p = ram_ld(desc_adr_sector+i, 1, mmem); p++; }

        uint32_t sector_len = desc.len;
        uint32_t sector_adr = (uint32_t)desc.adr;
        
        // (3) footer
        uint16_t desc_idx_footer = desc.next;
        uint32_t desc_adr_footer = desc_idx_footer * DESC_SIZE + qs->DescLow;
        p = (uint8_t*)&desc;
        for(int i=0; i<DESC_SIZE; i++){ *p = ram_ld(desc_adr_footer+i, 1, mmem); p++; }

        uint32_t footer_adr = (uint32_t)desc.adr;
 
        uint32_t request_size = 0;
        switch (header.type) {
        case VIRTIO_BLK_T_IN: { /////  disk -> dram
	    for(int i=0, j=0; i<(int)sector_len; i+=512, j++)
		read_sd(header.sector_num + j, sector_adr + i);
            ram_st(footer_adr, 0, 1, mmem); //  VIRTIO_BLK_S_OK
            break; }
        case VIRTIO_BLK_T_OUT: { ///// dram -> disk
	    for(int i=0, j=0; i<(int)sector_len; i+=512, j++)
		write_sd(header.sector_num + j, sector_adr + i);
	    ram_st(footer_adr, 0, 1, mmem); //  VIRTIO_BLK_S_OK
            //ram_st(sector_adr+sector_len-1, 0, 1, mmem); //  VIRTIO_BLK_S_OK FIXME ??
            break; }
        default: { simrv_puts("__ ERROR: disk unknown header "); simrv_puth(header.type); simrv_puts("\n"); simrv_exit(0); }
        }

        update_descriptor(desc_idx_header, request_size, q_num, qs, mmem);
        qs->last_avail_idx++;
    }
}

/******************************************************************************************/
/*** input from keyboard                                                               ****/
/******************************************************************************************/
void kbrd_request(uint8_t *mmem, uint32_t q_num, QueueState *qs, uint8_t buf){
    Descriptor desc;
    uint8_t *p;

    //buf = 'a';
    if (!qs->Ready) return;

    //simrv_puts("READY\n");
    //simrv_puth((uint32_t)qs); simrv_putc(0xa);
    //simrv_puth(qs->AvailLow+2); simrv_putc(0xa);

    uint16_t avail_idx = (uint16_t)ram_ld(qs->AvailLow+2, 2, mmem);
    //simrv_puth(avail_idx); simrv_putc(0xa);
    if (qs->last_avail_idx == avail_idx) return;
    //simrv_puts("AVAIL\n");
    uint32_t adr = qs->AvailLow + 4 + (qs->last_avail_idx & (q_num - 1)) * 2;
    uint16_t desc_idx_header = ram_ld(adr, 2, mmem);
    uint32_t desc_adr_header = desc_idx_header * DESC_SIZE + qs->DescLow;

    p = (uint8_t*)&desc;
    for(int i=0; i<DESC_SIZE; i++){ *p = ram_ld(desc_adr_header+i, 1, mmem); p++; }

    ram_st(desc.adr, (uint32_t)buf, 1, mmem); /*****/

    update_descriptor(desc_idx_header, 1, 2, qs, mmem);
    qs->last_avail_idx++;
    //simrv_puts("RET\n");
}

/******************************************************************************************/
void main(){
    // Mode (1: Console, 2: Disk)
    uint32_t *MODE = (uint32_t*)0x40009000;
    uint32_t mode = *MODE;

    // QNUM
    uint32_t* QNUM;
    QNUM = (uint32_t*)0x40009004;
    int qnum = *QNUM;
    
    // QSEL
    uint32_t* QSEL;
    QSEL = (uint32_t*)0x40009008;
    int idx = *QSEL;

    // Queues for Console
    QueueState *CONS_Q;
    CONS_Q = (QueueState *)0x4000a000;

    // Queues for Disk
    QueueState *DISK_Q;
    DISK_Q = (QueueState *)0x4000b000;

    // Keyboard input buffer
    uint8_t *CONS_FIFO = (uint8_t*)0x4000c000;
    int cons_fifo = *CONS_FIFO;

    if((idx > 1 && mode==1)){
        simrv_puts("ERROR! INDEX OVERFLOW MODE1\n");
    }
    else if((idx > 3 && mode==2)){
        simrv_puts("ERROR! INDEX OVERFLOW MODE2\n");
    }
    /*else if((idx > 0 && mode==3)){
        simrv_puts("ERROR! INDEX OVERFLOW MODE3\n");
    }*/

    QueueState* tc_queue = (idx==0) ? &CONS_Q[0] : &CONS_Q[1];
    QueueState* td_queue = (idx==0) ? &DISK_Q[0] : (idx==1) ? &DISK_Q[1] : (idx==2) ? &DISK_Q[2] : &DISK_Q[3];

    if(mode == 1){
        //simrv_puts("MODE1\n");
        cons_request(0, qnum, tc_queue);
    }
    else if(mode == 2){
        disk_request(0, (uint8_t *)0x90000000, qnum, td_queue);
    }
    else if(mode == 3){
        //simrv_puts("MODE3\n");
        kbrd_request(0, qnum, &CONS_Q[0], cons_fifo);
    }

    simrv_exit(0);
}
/******************************************************************************************/
