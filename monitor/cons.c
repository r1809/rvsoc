/*
 * RVSoC mini monitor -- console interface
 *
 * This source code is public domain
 *
 */
 
// virtio defs and register for the console output stream:
// - keyboard is channel 0, screen is channel 1
// - use only one buffer (2 is next power of two)
//
#define NUM 2
#include "virtio.h"

#define R(r) ((volatile uint32 *)(0x4000a000 + (r)))
#define KBD 0
#define SCN 1

static struct virtq_desc  kdb_buf, scn_buf;  
static struct virtq_avail kdb_drv, scn_drv; 
static struct virtq_used  kdb_dev, scn_dev;

static unsigned char key;

// init console keyboard virtio queue
//
void cons_init()
{
	// init keyboard queue
	*R(VIRTIO_MMIO_QUEUE_SEL)       = KBD;
	*R(VIRTIO_MMIO_QUEUE_DESC_LOW)  = (uint32) &kdb_buf;
	*R(VIRTIO_MMIO_DRIVER_DESC_LOW) = (uint32) &kdb_drv;
	*R(VIRTIO_MMIO_DEVICE_DESC_LOW) = (uint32) &kdb_dev;
	*R(VIRTIO_MMIO_QUEUE_NUM)       = NUM;

	// set up queue structure with one buffer
	kdb_buf.addr    = (unsigned int)&key;
	kdb_buf.len     = 1;
	kdb_buf.flags   = VRING_DESC_F_WRITE;
	kdb_drv.ring[0] = kdb_drv.ring[1] = 0;
	kdb_drv.idx++;

	*R(VIRTIO_MMIO_QUEUE_READY) = 0x1;
	
	// init screen queue
	*R(VIRTIO_MMIO_QUEUE_SEL)       = SCN;
	*R(VIRTIO_MMIO_QUEUE_DESC_LOW)  = (uint32) &scn_buf;
	*R(VIRTIO_MMIO_DRIVER_DESC_LOW) = (uint32) &scn_drv;
	*R(VIRTIO_MMIO_DEVICE_DESC_LOW) = (uint32) &scn_dev;
	*R(VIRTIO_MMIO_QUEUE_NUM)       = NUM;

	// set up queue structure with one buffer
	//scn_buf.addr    = (unsigned int)&key;
	//scn_buf.len     = 1;
	scn_buf.flags   = VRING_DESC_F_WRITE;
	scn_drv.ring[0] = scn_drv.ring[1] = 0;

	*R(VIRTIO_MMIO_QUEUE_READY) = 0x1;
}

void cons_rele()
{
	*R(VIRTIO_MMIO_QUEUE_SEL)   = KBD;
	*R(VIRTIO_MMIO_QUEUE_READY) = 0x0;
	*R(VIRTIO_MMIO_QUEUE_SEL)   = SCN;
	*R(VIRTIO_MMIO_QUEUE_READY) = 0x0;
}

void cons_write(char* s)
{
	unsigned int len;
	
	scn_buf.addr = (unsigned int)s;
	for(len=0; *s; len++) s++;
	scn_buf.len  = len;
	//scn_seen = scn_dev.idx;
	scn_drv.idx++;
	*R(VIRTIO_MMIO_QUEUE_NOTIFY) = SCN; // kick channel 1 into action
}

// send a character to the screen using the Uart directly
//
void putc(char c)
{
	int *uartTx = (int *)0x40008000;
	*uartTx = 0x10000 | c;
}

static unsigned int keyc, kflag, kbd_seen;

unsigned int getc(void)
{
	unsigned int *fp = &kflag; // work around prefetch
	unsigned int *kp = &keyc;  // work around prefetch
	while(1) {
		if(*fp) {
			*fp = 0;
			return *kp;
		}
	}
}

void cons_intr(void)
{
	unsigned char *kp = &key; // work around prefetch of 'key' into register
	
	//putw(kdb_dev.idx); putc(' '); putw(*kp); putc('\n');
	if(kdb_dev.idx!=kbd_seen) {
		kflag = 1; keyc = *kp;
		kdb_drv.idx++;
	}
	//putw(kflag); putc(' '); putw(keyc); putc('\n');
	*R(VIRTIO_MMIO_INTERRUPT_ACK) = 1;
}

unsigned int getline(char* buf, unsigned int sz)
{
	char c;
	unsigned int pos = 0;
	
	sz--;
	buf[sz] = 0;
	sz--;
	while(1) {
		c = getc();
		if(c == 0x7f || c == 0x08) { // DEL or BS
			if(pos == 0)
				continue;
			c = 0x08;
			putc(c); putc(' '); putc(c);
			pos--;
			continue;
		}
		if(c == 0x0d) {  // CR -> NL
			c = 0x0a;
		}
		putc(c);
		buf[pos++] = c;
		buf[pos] = 0;
		if(c == 0x0a || pos == sz)
			return pos;
	}
}
